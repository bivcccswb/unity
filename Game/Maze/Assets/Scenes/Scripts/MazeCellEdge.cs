﻿using UnityEngine;

public abstract class MazeCellEdge : MonoBehaviour {
	MazeCell cell, otherCell;
	MazeDirection direction;
	public void Initialize (MazeCell cell, MazeCell otherCell, MazeDirection direction) {
		this.cell = cell;
		this.otherCell = otherCell;
		this.direction = direction;
		cell.SetEdge(direction, this);
		transform.parent = cell.transform;
		transform.localPosition = Vector3.zero;
		transform.localRotation = direction.ToRotation();
	}
}
